people_urls = Cache.fetch(:imdb_people)
urls = people_urls.keys

query = "SELECT DISTINCT ?uni \n"
query << "where {\n"
query << urls.collect { |person| "{ <#{person}> <http://dbpedia.org/ontology/almaMater> ?uni }\n"}.join("UNION\n")
query << "}\n"

local = SPARQL::Client.new("http://localhost:2020/sparql")
results = local.query query

unis= results.collect(&:uni).collect(&:to_s)
p [unis, unis.length]

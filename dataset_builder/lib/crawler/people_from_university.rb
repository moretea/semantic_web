module Crawler
  class PeopleFromUniversity  < Base
    MAX_ATTEMPTS = 10
    DEFAULT_FAILURE_SLEEP_TIME = 5 # 5 seconds
    MAX_FAILURE_SLEEP_TIME     = 10 * 60 # Ten minutes

    # These are the occupations we're interested in
    OCCUPATIONS = %W{
      Actor
      Film_director
      Executive_producer
      Screenwriter
      Production_coordinator
      Stunt_coordinator
      Assistant_director
      Set_designer
      Set_decorator
      Costume_designer
      Make-up_artist
      Hairdresser
      Special_effects_supervisor
      Cinematographer
      Camera_operator
      Focus_puller
      Film_editor
      VFX_Creative_Director
    }

    def self.crawl university_urls
      self.new(university_urls).crawl
    end

    def initialize university_urls
      @university_urls = university_urls

      # Decorate urls with a hash, so we can store the number of attempts
      @work_queue = university_urls.map { |url| { url: url, attempt: 0 } }

      # Store the final results in here
      @results = {
        succeeded: [],
        failed: []
      }
      
      @sleep_at_failure = DEFAULT_FAILURE_SLEEP_TIME
    end


    def crawl
      start_operation! total: @work_queue.length
      while not @work_queue.empty?
        db = SPARQL::Client.new("http://dbpedia.org/sparql")

        print_eta! remaining: @work_queue.length

        item = @work_queue.shift # Take first from list

        uni_url = item[:url]

        begin
          OCCUPATIONS.each do |occupation|
            occupation_url = "http://dbpedia.org/resource/#{occupation}"
            @query =  "SELECT ?person WHERE {\n  "
            @query << "   ?person <http://dbpedia.org/ontology/almaMater> <#{uni_url}>.\n"
            @query << "   ?person <http://dbpedia.org/ontology/occupation> <#{occupation_url}>.\n"
            @query << "}"

            people_urls = db.query(@query).collect { |result| result.person}.collect(&:to_s)
            data = [uni_url, occupation, people_urls]
            p data if people_urls.any?
            @results[:succeeded] << { university: uni_url, occupation: occupation_url, persons: people_urls}
            sleep 0.1
            @sleep_at_failure = DEFAULT_FAILURE_SLEEP_TIME
          end
        rescue Exception => e
          item[:attempt] += 1
          if item[:attempt] < MAX_ATTEMPTS
            p [:encounted_exception, e, item, sleep: @sleep_at_failure]
            p @query
            @work_queue.push item
          else
            p [:failed, e, item]
            @results[:failed] << item
          end
          sleep @sleep_at_failure
          @sleep_at_failure *= 2 # Double it
          if @sleep_at_failure > MAX_FAILURE_SLEEP_TIME
            @sleep_at_failure = MAX_FAILURE_SLEEP_TIME
          end
        end
      end

      @results
    end
  end
end

require_relative "setup"

types = Cache.cache(:movie_actor_types) { Crawler::Movies.person_roles }

PREFIX = <<-prefix
  @prefix vocab: <http://semweb.moretea.nl/vocab/> .
  @prefix rankings_vocab: <http://semweb.moretea.nl/university_rankings/> .
  @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
  @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
  @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
  @prefix owl: <http://www.w3.org/2002/07/owl#> .
  @prefix lmdb: <http://data.linkedmdb.org/resource/movie/> .
  @prefix foaf: <http://xmlns.com/foaf/0.1/> .
prefix
# Make sure that all roles linkedmdb are
MOVIE_ROLE = "http://semweb.moretea.nl/vocab/MovieRole"
File.open("../rdf/movie_roles.ttl", "w") do |file|
  types.each do |name, url|
    print_triple file, url, "rdfs:subClassOf", MOVIE_ROLE
  end
end

people = Cache.fetch_cached :people

people_urls = Cache.cache(:imdb_people) { Crawler::Movies.imdb_better_people people }
imdb_people_urls = Cache.cache(:imdb_people_urls) { people_urls.collect { |db, lmdbs| lmdbs }.flatten }

imdb_people_with_roles = Cache.cache(:imdb_people_with_roles) { Crawler::Movies.imdb_people_with_roles  imdb_people_urls }
movie_urls = Cache.cache(:movie_urls) { imdb_people_with_roles.values.collect(&:values).flatten }
imdb_movies = Cache.cache(:imdb_movies) { Crawler::Movies.imdb_movies movie_urls }


LMDB_WROTE       = "http://data.linkedmdb.org/resource/movie/writer"
LMDB_ACTED_IN    = "http://data.linkedmdb.org/resource/movie/actor"
LMDB_CREW_OF     = "http://data.linkedmdb.org/resource/movie/film_crewmember"
LMDB_DIRECTOR_OF = "http://data.linkedmdb.org/resource/movie/director"
LMDB_EDITOR_OF   = "http://data.linkedmdb.org/resource/movie/editor"

# Set same URL's
File.open("../rdf/movie_people_same_as_dbpedia.ttl", "w") do |file|
  file.puts "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> ."
  # Write that people are the same.
  people_urls.each do |db, lmdbs|
    lmdbs.each do |lmdb|
      print_triple file, db, OWL_SAME_AS, lmdb
    end
  end

  file.puts "\n\n"
  
  # Write the roles that people have in movies
  imdb_people_with_roles.each do |person, roles|
    roles[:wrote].each do |movie|
      print_triple file, person, LMDB_WROTE, movie
    end

    roles[:actedIn].each do |movie|
      print_triple file, person, LMDB_ACTED_IN, movie
    end

    roles[:crewOf].each do |movie|
      print_triple file, person, LMDB_CREW_OF, movie
    end

    roles[:directorOf].each do |movie|
      print_triple file, person, LMDB_DIRECTOR_OF, movie
    end

    roles[:editorOf].each do |movie|
      print_triple file, person, LMDB_EDITOR_OF, movie
    end
  end

  file.puts "\n\n"

  # Write information about the movies

  imdb_movies.each do |movie|
    print_triple file, movie.first, RDF_A, "http://data.linkedmdb.org/resource/movie/film"
    file.puts("<#{movie.first}> <#{DC_TITLE}> \"#{movie.last[:title].to_s}\"^^xsd:string .")
  end
end

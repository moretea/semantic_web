require_relative "setup"

require "youtube_search"
imdb_movies = Cache.fetch_cached(:imdb_movies)

File.open("../rdf/youtube_trailers.ttl", "w") do |file|
  file.puts <<-PREFIX
    @prefix vocab: <http://semweb.moretea.nl/vocab/> .
    @prefix rankings_vocab: <http://semweb.moretea.nl/university_rankings/> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    @prefix lmdb: <http://data.linkedmdb.org/resource/> .
  PREFIX
  imdb_movies.each do |movie_url, values|
    title = values[:title]
    q = "#{title} official trailer"
    p q
    trailer = YoutubeSearch.search(q).first

    if trailer
      file.puts("<#{movie_url}> vocab:youtubeTrailer \"#{trailer['video_id']}\"^^xsd:string .")
    end
  end
end

require "rubygems"
require "bundler"
Bundler.setup
require "sparql/client"

Dir["lib/**/*"].sort.each do |lib|
  file = File.join(Dir.pwd, lib)
  if File.file?(file)
    require_relative(file) 
  end
end

RDF_A = "a"
OWL_SAME_AS = "http://www.w3.org/2002/07/owl#sameAs"
FOAF_PERSON = "http://xmlns.com/foaf/0.1/Person"

DC_TITLE = "http://purl.org/dc/terms/title"

def print_triple file, object, predicate, subject
  object    = "<#{object}>" unless object.start_with?("<")
  predicate = "<#{predicate}>" unless predicate == RDF_A
  subject   = "<#{subject}>"

  file.puts [object, predicate, subject].join(" ")  + ".\n"
end

class PeopleController < ApplicationController
  def index
    if params[:q]
      @people = Person.search params[:q]
    else
      @people = Person.all
    end

    if params[:replace] == 'true'
      render partial: "people/people_list_items"
    end
  end

  def show
    id = CGI.unescape(params[:id])
    @person = Person.find(id)
    @universities = @person.almaMater.collect do |url|
      University.find(url)
    end
  end
end

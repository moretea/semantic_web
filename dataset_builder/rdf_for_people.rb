require_relative "setup"

uni_urls    = Cache.cache(:uni_urls)    { Crawler::University.crawl }
person_urls = Cache.cache(:person_urls) { Crawler::PeopleFromUniversity.crawl(uni_urls) }
person_urls = person_urls[:succeeded].select { |x| x[:persons].length > 0 }
people      = Cache.cache(:people)      { person_urls.collect { |x| x[:persons] }.flatten }


ALMA_MATER = "http://dbpedia.org/ontology/almaMater"
OCCUPATION = "http://dbpedia.org/ontology/occupation"

File.open("../rdf/people.ttl", "w") do |file|
  # Print triples for people
  person_urls.each do |results|
    university = results[:university]
    occupation = results[:occupation]
    persons    = results[:persons]

    persons.each do |person|
      print_triple file, person, RDF_A, FOAF_PERSON
      print_triple file, person, ALMA_MATER, university
      print_triple file, person, OCCUPATION, occupation
    end
  end
end

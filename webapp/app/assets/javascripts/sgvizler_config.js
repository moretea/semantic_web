  // CONFIGURATION Sgvizler 0.5: Set variables to fit your setup.
  // NB! Do not let the last item in a list end with a comma.

   //// Query settings. The defaults settings are listed.
   sgvizler.option.query = {
       // Default query. 
       //'query':                "SELECT ?class (count(?instance) AS ?noOfInstances)\nWHERE{ ?instance a ?class }\nGROUP BY ?class\nORDER BY ?class",

       // Endpoint URL. 
       //'endpoint':             "http://sws.ifi.uio.no/sparql/world",
       'endpoint':             "/sparql",

       // Endpoint output format. 
       //'endpoint_output':      'json',  // 'json', 'jsonp' or 'xml'.
       'endpoint_output':      'json',  // 'xml' or 'json' 

       // This string is appended the 'endpoint' variable and the query to it again to give a link to the "raw" query results.
       //'endpoint_query_url':   "?output=text&amp;query=",
       'endpoint_query_url':   "?output=text&amp;query=",

       // URL to SPARQL validation service. The query is appended to it. 
       //'validator_query_url':  "http://www.sparql.org/query-validator?languageSyntax=SPARQL&amp;outputFormat=sparql&amp;linenumbers=true&amp;query=",

       // Default chart type. 
       //'chart':                'gLineChart',

       // Default log level. Must be either 0, 1, or 2. 
       //'loglevel':             2
   };

   //// Prefixes
   // Add convenient prefixes for your dataset. rdf, rdfs, xsd, owl
   // are already set.  Examples: 
   sgvizler.option.namespace['sw']  = 'http://semweb.moretea.nl/';
   sgvizler.option.namespace['rankings_vocab']  = 'http://semweb.moretea.nl/university_rankings/';
   sgvizler.option.namespace['geo']  = 'http://www.w3.org/2003/01/geo/wgs84_pos#';

   //// Your chart drawing preferences. The defaults are listed.
   // See the Google visualization API for available options for
   // Google charts, and the Sgvizler homepage for other
   // options. Options applicable to all charts are put in the
   // "root" of sgvizler.chartOptions. Chart specific options are
   // put in a "child" with the chart's id as name,
   // e.g. 'gGeoMap'. 
   sgvizler.option.chart = { 
       //'width':           '800',
       //'height':          '400',
       //'chartArea':       { left: '5%', top: '5%', width: '75%', height: '80%' },
       //     'gGeoMap': {
       //        'dataMode':           'markers'
       //     },
       //     'gMap': {
       //        'dataMode':           'markers',
       //     },
       //     'sMap': {
       //        'dataMode':           'markers',
       //        'showTip':            true,
       //        'useMapTypeControl':  true
       //     } 
   };

   //// Leave this as is. Ready, steady, GO!
   $(document).ready(sgvizler.go());

module Crawler
  class Base
    def start_operation! options
      @start_at = Time.now.to_i.to_f
      @total_objects = options[:total]
    end

    def print_eta! options
      now = Time.now.to_i.to_f
      objects_remaining = options[:remaining]
      objects_processed = @total_objects - objects_remaining

      total_time = now - @start_at
      time_per_item = total_time / (objects_processed + 1).to_f

      time_remaining = (time_per_item * objects_remaining).to_i

      minutes = time_remaining / 60
      seconds = time_remaining % 60

      hours   = minutes        / 60
      minutes = minutes        % 60

      puts "[%3d of %3d] ETA: %dh:%dm:%ds" % [objects_processed, @total_objects, hours, minutes, seconds]
    end
  end
end

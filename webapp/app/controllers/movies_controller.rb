class MoviesController < ApplicationController
  def index
    if params[:q]
      @movies = Movie.search params[:q]
    else
      @movies = Movie.all
    end

    if params[:replace] == 'true'
      render partial: "movies/movie_list_items"
    end
  end

  def show
    id = CGI.unescape(params[:id])
    @movie = Movie.find(id)

   # @people = @movie.participants.collect do |url|
   #   Person.find(url)
   # end
    #
   @people = Person.all[10..20]
  end
end

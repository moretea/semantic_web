require_relative "setup"

uni_urls = Cache.cache(:uni_urls) { Crawler::University.crawl }

rdf = Cache.cache(:uni_geo) { Crawler::University.geo_location_rdf(uni_urls) }

File.open("../rdf/university_geo.ttl", "w") do |f|
  f.puts rdf
end

require_relative "setup.rb"
endpoint = ARGV.pop
raise "No endpoint given!" unless endpoint

$client = SPARQL::Client.new(endpoint)

require "irb"
IRB.start

class UniversitiesController < ApplicationController
  def index
    if params[:q]
      @universities = University.search params[:q]
    else
      @universities = University.all
    end

    if params[:replace] == 'true'
      render partial: "universities/university_list_items"
    end
  end

  def show
    id = CGI.unescape(params[:id])
    @university = University.find(id)
    @people = Person.with_alma_mater(id)
  end
end

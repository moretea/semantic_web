Webapp::Application.routes.draw do
  root to: "map#index"

  match "/universities/*id", to: "universities#show", constraints: { :format => /(html|xml|json)/ }
  resources :universities, only: [:index, :show]

  match "/people/*id", to: "people#show", constraints: { :format => /(html|xml|json)/ }
  resources :people, only: [:index, :show]

  match "/movies/*id", to: "movies#show", constraints: { :format => /(html|xml|json)/ }
  resources :movies, only: [:index, :show]
end

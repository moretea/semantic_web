require "sparql/client"

class University
  ENDPOINT = "http://localhost:2020/sparql"

  PREFIXES = <<-EOP
    prefix vocab: <http://semweb.moretea.nl/vocab/> 
    prefix rankings_vocab: <http://semweb.moretea.nl/university_rankings/> 
    prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
    prefix owl: <http://www.w3.org/2002/07/owl#> 
  EOP

  attr_accessor :attributes
  
  def initialize(url)
    @url = url
    @attributes = {}
  end

  def id
    @url
  end

  def to_param
    CGI.escape(@url)
  end

  def method_missing key, *args, &block
    if ATTRIBUTES.keys.member? key
      @attributes[key]
    else
      super
    end
  end

  ATTRIBUTES = { 
      geo_lat:  "http://www.w3.org/2003/01/geo/wgs84_pos#lat",
      geo_long: "http://www.w3.org/2003/01/geo/wgs84_pos#long",
      label: "http://www.w3.org/2000/01/rdf-schema#label",
      nr_students: "http://dbpedia.org/property/students"
  }

  ATTR_INV = ATTRIBUTES.invert

  class << self
    def type_name
      "rankings_vocab:University"
    end

    def search q
      match = <<-SPARQL
        {
          ?match rdf:type #{self.type_name} .
          ?match rdfs:label ?label .
          FILTER REGEX(str(?label), "#{q}", "i")
        }
      SPARQL

      attribute_parts = []
      search_attributes =  ATTRIBUTES.reject { |k, v| k == :label}
      search_attributes.values.each_with_index do |attr, idx| 
        attribute_parts << <<-SPARQL
          {
            ?match ?p#{idx} ?o#{idx} .
            FILTER(?p#{idx} = <#{attr}>)
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      query_results = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      results = {}

      query_results.each do |result|
        # Find objects
        object = results[result.match.to_s] ||= self.new(result.match.to_s)
        object.attributes[:label] = result.label

        search_attributes.each_with_index do |attr, idx|
          # If set, set relation
          pred_key = "p#{idx}".to_sym
          if result[pred_key]
            p = result[pred_key].to_s
            obj_key = "o#{idx}".to_sym
            object.attributes[ATTR_INV[p] || p] = decode(result[obj_key])
          end
        end
      end

      results.values
    end

    def find url
      match = <<-SPARQL
        {
          ?match rdf:type #{self.type_name} .
          FILTER(?match = <#{url}>)
        }
      SPARQL

      attribute_parts = []
      ATTRIBUTES.values.each_with_index do |attr, idx| 
        attribute_parts << <<-SPARQL
          {
            ?match ?p#{idx} ?o#{idx} .
            FILTER(?p#{idx} = <#{attr}>)
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      result = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      decorate(result, type: self.type_name).first
    end

    def all
      match = <<-SPARQL
        {
          ?match rdf:type #{self.type_name} .
        }
      SPARQL

      attribute_parts = []
      ATTRIBUTES.values.each_with_index do |attr, idx| 
        attribute_parts << <<-SPARQL
          {
            ?match ?p#{idx} ?o#{idx} .
            FILTER(?p#{idx} = <#{attr}>)
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      result = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      decorate(result, type: self.type_name)
    end

    protected

    def query q
      sparql_query = PREFIXES + "\n" + q 

      Rails.logger.debug sparql_query
      client.query(sparql_query)
    end

    def decorate query_results, type
      results = {}

      query_results.each do |result|
        # Find objects
        object = results[result.match.to_s] ||= self.new(result.match.to_s)

        ATTRIBUTES.each_with_index do |attr, idx|
          # If set, set relation
          pred_key = "p#{idx}".to_sym
          if result[pred_key]
            p = result[pred_key].to_s
            obj_key = "o#{idx}".to_sym
            object.attributes[ATTR_INV[p] || p] = decode(result[obj_key])
          end
        end
      end

      results.values
    end

    DECODE_DATATYPES = {
      "http://www.w3.org/2001/XMLSchema#float" => lambda { |val| val.to_s.to_f },
      "" => lambda { |val| val.value } # string
    }
    def decode(thing)
      if thing.kind_of? RDF::Literal
        type = thing.datatype.to_s
        method = DECODE_DATATYPES[type]
        debugger if method.nil?
        raise "Unkown datatype datatype:#{type} thing:#{thing.inspect}" if method.nil?
        method.call(thing)
      else
        thing
      end
    end

    def client
      SPARQL::Client.new(ENDPOINT)
    end
  end
end

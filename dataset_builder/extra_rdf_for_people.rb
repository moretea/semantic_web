require_relative "setup"

people = Cache.fetch_cached(:people)

client = SPARQL::Client.new("http://dbpedia.org/sparql")

GIVEN_NAME = "<http://xmlns.com/foaf/0.1/givenName>"
SUR_NAME = "<http://xmlns.com/foaf/0.1/surname>"
RDFS_COMMENT = "<http://www.w3.org/2000/01/rdf-schema#comment>"
DB_THUMBNAIL = "<http://dbpedia.org/ontology/thumbnail>"

File.open("../rdf/extra_person_rdf.ttl", "w") do |file|
  people.each_with_index do |person, idx|
    p [idx, :of, people.length]

    results = client.query <<-SPARQL
      CONSTRUCT {
          <#{person}> #{GIVEN_NAME} ?firstName .
          <#{person}> #{SUR_NAME} ?lastName .
          <#{person}> #{RDFS_COMMENT}  ?comment .
          <#{person}> #{DB_THUMBNAIL} ?thumbnail .
      }
      WHERE {
        OPTIONAL {
          <#{person}> #{GIVEN_NAME} ?firstName .
          <#{person}> #{SUR_NAME} ?lastName .
        }
        OPTIONAL {
          <#{person}> #{RDFS_COMMENT}  ?comment .
          FILTER(lang(?comment) = "en")
        }
        OPTIONAL {
          <#{person}> #{DB_THUMBNAIL} ?thumbnail .
        }
      }
    SPARQL

    results.each_statement do |statement|
      file.puts [statement.subject, statement.predicate, statement.object].collect(&:to_ntriples).join(" ") << " .\n"
    end

    sleep 0.6
  end
end

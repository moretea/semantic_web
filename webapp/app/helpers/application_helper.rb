module ApplicationHelper
  def sgvzlr opts
    html_attrs = {
      "data-sgvizler-query" => opts[:query],
      "data-sgvizler-chart" => opts[:chart],
      "id"                  => opts[:id],
      "style" => "width:100%;height:500px;"
    }

    tag :div, html_attrs
  end
end

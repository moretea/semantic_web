class MapController < ApplicationController
  def index
    @query = <<-QUERY
     SELECT ?lat ?long ?url
     WHERE { 
       ?url a rankings_vocab:University ; 
           geo:lat ?lat;
           geo:long ?long . 
     }
    QUERY
  end
end

module Crawler
  class Movies 
    def self.person_roles
      local = SPARQL::Client.new("http://data.linkedmdb.org/sparql")
      query = local.query <<-QUERY
      select DISTINCT ?type
      WHERE
      {
        ?person a <http://xmlns.com/foaf/0.1/Person> .
        ?person a ?type .
        FILTER (!(?type= <http://xmlns.com/foaf/0.1/Person>))
      }
      QUERY
      Hash[*query.collect(&:type).collect(&:to_s).collect do |url|
        human_name = url.split("/").last

        [human_name.to_sym, url]
      end.flatten]
    end

    OWL_PREFIX= "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
    def self.imdb_people people
      local = SPARQL::Client.new("http://data.linkedmdb.org/sparql")

      imdb_people_urls = {}
      people.each_with_index do |person_url, idx|
        sparql = OWL_PREFIX + "select ?o WHERE\n { ?o owl:sameAs <#{person_url}> } \n"
        query = local.query sparql

        p [idx, :of, people.length]
        query.each { |result| imdb_people_urls[person_url] = result[:o].to_s }
      end

      imdb_people_urls
    end

    def self.imdb_better_people people
      same_as = {}
      possible_matches = people.collect { |x| [x, x.split("/").last.split("_").reject { |x| x.end_with?(".")}]}
      i = 0;
      possible_matches.each do |db, name_parts|
        i+=1 

        p [i, :of, possible_matches.length, "   ", db, name_parts]

        client = SPARQL::Client.new("http://data.linkedmdb.org/sparql")

        regexes = name_parts.collect { |part| "REGEX(STR(?label), '#{part.gsub(/[^a-zA-Z]/, '')}', 'i')" }
         sparql_query = <<-SPARQL
          SELECT *
          WHERE
          {
            ?object <http://www.w3.org/2000/01/rdf-schema#label> ?label .
            FILTER( #{regexes.join(" && ")} )
          }
        SPARQL

        #puts sparql_query

        results = client.query sparql_query

        results.each do |result|
          same_as[db] ||= []
          same_as[db] << result.object

          p [db, :same_as, result.object]
        end

        sleep 0.6
      end

      same_as
    end

    def self.imdb_people_with_roles imdb_people
      output = {}
      client = SPARQL::Client.new("http://data.linkedmdb.org/sparql")
      imdb_people.each_with_index do |lmdb, idx|
        p [idx, :of, imdb_people.length]

        person_url = lmdb.to_ntriples
        results = client.query <<-SPARQL
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
          prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix foaf: <http://xmlns.com/foaf/0.1/>

          SELECT *
          WHERE {
             OPTIONAL { ?wrote      <http://data.linkedmdb.org/resource/movie/writer>           #{person_url} . }
             OPTIONAL { ?actedIn    <http://data.linkedmdb.org/resource/movie/actor>            #{person_url} . }
             OPTIONAL { ?crewOf     <http://data.linkedmdb.org/resource/movie/film_crewmember>  #{person_url} . }
             OPTIONAL { ?directorOf <http://data.linkedmdb.org/resource/movie/director>         #{person_url} . }
             OPTIONAL { ?editorOf   <http://data.linkedmdb.org/resource/movie/editor>           #{person_url} . }
          }
        SPARQL

        results.each do |result|
          output[person_url] ||= { wrote: [], actedIn: [], crewOf: [], directorOf: [], editorOf: []}

          result.to_hash.each do |key, value|
            output[person_url][key] << value.to_s
          end
        end

        sleep 0.6
      end

      output
    end

    def self.imdb_movies movie_urls
      output = {}
      client = SPARQL::Client.new("http://data.linkedmdb.org/sparql")
      movie_urls.each_with_index do |url, idx|
        p [idx, :of, movie_urls.length]

        results = client.query <<-SPARQL
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
          prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix foaf: <http://xmlns.com/foaf/0.1/>

          SELECT *
          WHERE {
            <#{url}> <http://purl.org/dc/terms/title> ?title .
             OPTIONAL { <#{url}> foaf:page ?imdb_page. 
                        FILTER( REGEX(str(?imdb_page), "imdb.com") ) }
             OPTIONAL { <#{url}> <http://data.linkedmdb.org/resource/movie/runtime> ?runtime. }
          }
        SPARQL

        results.each do |result|
          output[url] ||= { imdb_page: nil, runtime: nil }

          result.to_hash.each do |key, value|
            output[url][key] = value.to_s
          end
        end
        p output[url]

        sleep 1
      end

      output
    end
  end
end

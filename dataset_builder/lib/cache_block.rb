module Cache
  extend self

  def cache id, &block
    if is_cached? id
      p [:use_cached, id]
      fetch_cached id
    else
      p [:generate_cache_for, id]
      result = block.call
      write_cache! id, result
      result
    end
  end

  def cached_name_for id
    "cached/#{id}"
  end

  def is_cached? id
    File.exists? cached_name_for id
  end

  def fetch_cached id
    Marshal.load File.read cached_name_for id
  end

  def write_cache! id, value
    File.write(cached_name_for(id), Marshal.dump(value))
  end
end

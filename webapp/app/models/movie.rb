require "sparql/client"

class Movie
  ENDPOINT = "http://localhost:2020/sparql"

  PREFIXES = <<-EOP
    prefix vocab: <http://semweb.moretea.nl/vocab/> 
    prefix rankings_vocab: <http://semweb.moretea.nl/university_rankings/> 
    prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
    prefix owl: <http://www.w3.org/2002/07/owl#> 
    prefix foaf: <http://xmlns.com/foaf/0.1/>
    prefix lmdb: <http://data.linkedmdb.org/resource/movie/>
  EOP

  attr_accessor :attributes
  
  def initialize(url)
    @url = url
    @attributes = {}
  end

  def id
    @url
  end

  def to_param
    CGI.escape(@url)
  end

  def method_missing key, *args, &block
    if ATTRIBUTES.keys.member? key
      @attributes[key]
    else
      super
    end
  end

  ATTRIBUTES = { 
    title:    { url: "http://purl.org/dc/terms/title",      type: :one },
    runtime:  { url: "http://data.linkedmdb.org/resource/movie/runtime",    type: :one },
    imdb_page:  { url: "foaf:page", type: :one },
    youtube_id:  { url: "vocab:youtubeTrailer", type: :one },
    participants:  { url: "vocab:InvMovieRole", type: :many }
  }

  ATTR_INV = ATTRIBUTES.invert

  class << self
    def type_name
      "lmdb:film"
    end

    def search q
      match = <<-SPARQL
        {
          ?match a #{type_name} .
          ?match <#{ATTRIBUTES[:title][:url]}> ?match_title.
          FILTER( regex(str(?match_title), "#{q}", "i") )
        }
      SPARQL

      attribute_parts = []
      ATTRIBUTES.each do |attr_name, attr_info|
        if attr_info[:url].start_with?("http")
          prop_url = "<#{attr_info[:url]}>"
        else
          prop_url = attr_info[:url]
        end

        attribute_parts << <<-SPARQL
          {
            ?match ?#{attr_name} ?#{attr_name}_value .
            FILTER(?#{attr_name} = #{prop_url})
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      result = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      decorate(result, type: self.type_name)
    end


    def find url
      match = <<-SPARQL
        {
          ?match a #{type_name} .
          OPTIONAL { ?match <http://www.w3.org/2002/07/owl#sameAs> ?c }
          FILTER(!bound(?c))
          FILTER(?match = <#{url}>)
        }
      SPARQL

      attribute_parts = []
      ATTRIBUTES.each do |attr_name, attr_info|
        if attr_info[:url].start_with?("http")
          prop_url = "<#{attr_info[:url]}>"
        else
          prop_url = attr_info[:url]
        end

        attribute_parts << <<-SPARQL
          {
            ?match ?#{attr_name} ?#{attr_name}_value .
            FILTER(?#{attr_name} = #{prop_url})
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      result = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      decorate(result, type: self.type_name).first
    end

    def all
      match = <<-SPARQL
        {
          ?match a #{type_name} .
        }
      SPARQL

      attribute_parts = []
      ATTRIBUTES.each do |attr_name, attr_info|
        if attr_info[:url].start_with?("http")
          prop_url = "<#{attr_info[:url]}>"
        else
          prop_url = attr_info[:url]
        end

        attribute_parts << <<-SPARQL
          {
            ?match ?#{attr_name} ?#{attr_name}_value .
            FILTER(?#{attr_name} = #{prop_url})
          }
        SPARQL
      end 

      parts = [match] + attribute_parts
      result = query <<-QUERY
        SELECT *
        WHERE {
          #{parts.join("OPTIONAL")}
        }
      QUERY

      decorate(result, type: self.type_name)
    end

    protected

    def query q
      sparql_query = PREFIXES + "\n" + q 

      Rails.logger.debug sparql_query
      client.query(sparql_query)
    end

    def decorate query_results, type
      results = {}

      query_results.each do |result|
        result = Hash[result.entries]
        # Find objects
        object = results[result[:match].to_s] ||= self.new(result[:match].to_s)

        value_keys = result.keys.select { |key| key.to_s.ends_with? "_value" }
        value_keys.each do |value_key|
          key = value_key.to_s.gsub("_value","").to_sym
          attr = ATTRIBUTES[key]
          value = decode(result[value_key])

          case attr[:type]
            when :one then  object.attributes[key] = value
            when :many then (object.attributes[key] ||= []).push(value)
          end
        end
      end

      results.values
    end

    DECODE_DATATYPES = {
      "http://www.w3.org/2001/XMLSchema#float" => lambda { |val| val.to_s.to_f },
      "http://www.w3.org/2001/XMLSchema#string" => lambda { |val| val.value }, # string
      "" => lambda { |val| val.value } # string
    }
    def decode(thing)
      if thing.kind_of? RDF::Literal
        type = thing.datatype.to_s
        method = DECODE_DATATYPES[type]
        debugger if method.nil?
        raise "Unkown datatype datatype:#{type} thing:#{thing.inspect}" if method.nil?
        method.call(thing)
      else
        thing
      end
    end

    def client
      SPARQL::Client.new(ENDPOINT)
    end
  end
end

module Crawler
  class University
    def self.crawl
      local = SPARQL::Client.new("http://localhost:2020/sparql")
      query = local.query <<-QUERY
        select ?uni where
        {
          ?x <http://semweb.moretea.nl/university_rankings/isRankingOf> ?uni .
        }
      QUERY

      query.collect { |result| result.uni }.collect &:to_s
    end


    def self.geo_location_rdf university_urls
      local = SPARQL::Client.new("http://dbpedia.org/sparql")

      rdf = ""

      university_urls.each_with_index do |university, idx|
        p [idx, :of, university_urls.length]

        query = local.query <<-QUERY
          CONSTRUCT { 
            <#{university}> geo:long ?long;
                     geo:lat  ?lat;
                     rdfs:label ?label;
                     <http://semweb.moretea.nl/university_rankings/numberOfStudents> ?nr_students .
          } WHERE {
            <#{university}> rdfs:label ?label.
            filter (lang(?label) = "en" )

            OPTIONAL {
              <#{university}> geo:long ?long.
              <#{university}> geo:lat  ?lat .
            }
                     
            OPTIONAL {
              <#{university}> <http://dbpedia.org/property/students> ?nr_students .
            }
          }
        QUERY


        query.each_statement do |statement|
          this_rdf = [statement.subject, statement.predicate, statement.object].collect(&:to_ntriples).join(" ") << " .\n"
          p this_rdf
          rdf << this_rdf
        end

        sleep 0.6
      end

      rdf
    end
  end
end

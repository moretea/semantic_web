class ApplicationController < ActionController::Base
  protect_from_forgery

  after_filter :set_access_control_headers

  def set_access_control_headers 
    headers['Access-Control-Allow-Origin'] = "*"
    headers['Access-Control-Request-Method'] = '*' 
  end

  PREFIXES =  <<-PREFIX
    vocab: http://semweb.moretea.nl/vocab/ 
    rankings_vocab: http://semweb.moretea.nl/university_rankings/ 
    rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# 
    rdfs: http://www.w3.org/2000/01/rdf-schema# 
    xsd: http://www.w3.org/2001/XMLSchema# 
    owl: http://www.w3.org/2002/07/owl# 
    foaf: http://xmlns.com/foaf/0.1/ 
  PREFIX
end
